/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerarreglos;

import java.util.Arrays;
import javax.swing.JOptionPane;
import tallerarreglos.modelo.Utilidad;

/**
 *
 * @author Oswaldo
 */
public class TallerArreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ejercicio1();
        ejercicio2();
        ejercicio3();
        ejercicio4();
        ejercicio5();
        ejercicio6();
        ejercicio7();
        ejercicio8();
        ejercicio9();

    }

    public static void ejercicio1() {

        Utilidad arreglo = new Utilidad();

        arreglo.numeros = new int[10];

        arreglo.numeros[0] = 49;
        arreglo.numeros[1] = 71;
        arreglo.numeros[2] = 53;
        arreglo.numeros[3] = 38;
        arreglo.numeros[4] = 113;
        arreglo.numeros[5] = 59;
        arreglo.numeros[6] = 71;
        arreglo.numeros[7] = 63;
        arreglo.numeros[8] = 448;
        arreglo.numeros[9] = 130;

        JOptionPane.showMessageDialog(null, "La posición del número mayor es: " + arreglo.obtenerPosicionNumeroMayor());
    }

    public static void ejercicio2() {

        Utilidad arreglo = new Utilidad();

        arreglo.numeros = new int[10];

        arreglo.numeros[0] = 31;
        arreglo.numeros[1] = 37;
        arreglo.numeros[2] = 3;
        arreglo.numeros[3] = 5;
        arreglo.numeros[4] = 7;
        arreglo.numeros[5] = 11;
        arreglo.numeros[6] = 13;
        arreglo.numeros[7] = 17;
        arreglo.numeros[8] = 23;
        arreglo.numeros[9] = 29;

        arreglo.determinarPrimo();
        System.out.println(arreglo.mostrarDatosNumerosPrimos());
    }

    public static void ejercicio3() {

        Utilidad arreglo = new Utilidad();

        arreglo.numeros = new int[10];

        arreglo.numeros[0] = 31;
        arreglo.numeros[1] = 37;
        arreglo.numeros[2] = 3;
        arreglo.numeros[3] = 5;
        arreglo.numeros[4] = 7;
        arreglo.numeros[5] = 11;
        arreglo.numeros[6] = 13;
        arreglo.numeros[7] = 17;
        arreglo.numeros[8] = 23;
        arreglo.numeros[9] = 29;
        
        arreglo.encontrarPrimosEntre100_300();

    }

    public static void ejercicio4() {

        Utilidad arreglo = new Utilidad();

        arreglo.numerosEnteros = new int[10];

        arreglo.llenarTerminadosEnNumero(10, 4);
        JOptionPane.showMessageDialog(null, arreglo.mostrarDatos());

    }

    public static void ejercicio5() {

        Utilidad arreglo = new Utilidad();

        arreglo.numeros = new int[10];

        arreglo.numeros[0] = 448;
        arreglo.numeros[1] = 71;
        arreglo.numeros[2] = 53;
        arreglo.numeros[3] = 448;
        arreglo.numeros[4] = 113;
        arreglo.numeros[5] = 59;
        arreglo.numeros[6] = 448;
        arreglo.numeros[7] = 63;
        arreglo.numeros[8] = 448;
        arreglo.numeros[9] = 130;

        JOptionPane.showMessageDialog(null, "El número mayor está repetido: " + arreglo.determinarNumMayorRepetido() + " veces.");

    }

    public static void ejercicio6() {

        Utilidad arreglo = new Utilidad();
        int[] arreglo1 = {47, 54, 6, 98, 43, 109, 85, 76, 220, 10};
        arreglo.numeros = arreglo1;
        arreglo.determinarPromedioEnArreglo();
        System.out.println(arreglo.determinarPromedioEnArreglo());
    }

    public static void ejercicio7() {

        Utilidad arreglo = new Utilidad();

        int[] arreglo1 = {47, 54, 6, 98, 43, 109, 85, 76, 220, 10};
        arreglo.numeros = arreglo1;

        int[] suma = arreglo.obtenerSumaDigitosPorPosicion();
        arreglo.numeros = suma;
        JOptionPane.showMessageDialog(null, "La posición del número cuya suma de digitos es la mayor es: " + arreglo.obtenerPosicionNumeroMayor());

    }

    public static void ejercicio8() {

        Utilidad arreglo = new Utilidad();

        arreglo.factorial = new long[10];

        arreglo.factorial[0] = 1;
        arreglo.factorial[1] = 2;
        arreglo.factorial[2] = 3;
        arreglo.factorial[3] = 4;
        arreglo.factorial[4] = 5;
        arreglo.factorial[5] = 6;
        arreglo.factorial[6] = 7;
        arreglo.factorial[7] = 8;
        arreglo.factorial[8] = 9;
        arreglo.factorial[9] = 10;

        arreglo.determinarFactorial();
        System.out.println(arreglo.mostrarDatosFactorial());

    }

    public static void ejercicio9() {

        Utilidad arreglo = new Utilidad();

        arreglo.numeros = new int[10];

        arreglo.numeros[0] = 23;
        arreglo.numeros[1] = 91;
        arreglo.numeros[2] = 84;
        arreglo.numeros[3] = 57;
        arreglo.numeros[4] = 76;
        arreglo.numeros[5] = 59;
        arreglo.numeros[6] = 44;
        arreglo.numeros[7] = 3;
        arreglo.numeros[8] = 22;
        arreglo.numeros[9] = 1;

        arreglo.mostrarDatosPuntoNueve();
    }

}
