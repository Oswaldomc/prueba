/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerarreglos.modelo;

/**
 *
 * @author Oswaldo
 */
public class Utilidad {

    public int[] numeros;
    public int[] numerosEnteros;
    public long[] factorial;

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    public String mostrarDatos() {

        String texto = "";
        for (int i = 0; i < numerosEnteros.length; i++) {

            texto = texto + "\nNumero[" + i + "]: " + numerosEnteros[i];
        }
        return texto;
    }

    public int obtenerPosicionNumeroMayor() {
        int pos = 0;
        int mayor = numeros[0];
        for (int i = 1; i < numeros.length; i++) {

            if (numeros[i] > mayor) {
                pos = i;
                mayor = numeros[i];
            }
        }
        return pos;
    }

    public void determinarPrimo() {

        int j = -1;
        int aux = -1;
        int mayor = -1;
        int posMayor = -1;

        for (int i = 0; i < numeros.length; i++) {
            j = 1;
            aux = 0;
            while (j <= numeros[i]) {
                if (numeros[i] % j == 0) {
                    aux++;
                }
                j++;
            }
            if (aux == 2) {
                if (mayor < numeros[i]) {
                    mayor = numeros[i];
                    posMayor = i;
                }
            }

        }
        if (mayor != -1) {
            System.out.print("El número primo mayor es: " + mayor + " en la posicion: " + posMayor);
        } else {
            System.out.print("No hay números primos en el arreglo.\n");
        }
    }

    public String mostrarDatosNumerosPrimos() {

        String texto = "";
        for (int i = 0; i < numeros.length; i++) {

            texto = texto + "\nNumero[" + i + "]: " + numeros[i];
        }
        return texto;
    }

    public void llenarTerminadosEnNumero(int cuantos, int termina) {

        numerosEnteros = new int[cuantos];
        for (int i = 0; i < cuantos; i++) //cuantos = numerosEnteros.lenght
        {
            numerosEnteros[i] = (i * 10) + termina;
        }

    }

    public void encontrarPrimosEntre100_300() {

        int aux;
        int ultimo;

        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                ultimo = 99;
            } else {
                ultimo = numeros[i - 1];
            }

            for (int j = ultimo + 2; j < 300; j += 2) {
                aux = 0;
                for (int k = 2; k < j; k++) {
                    if (j % k == 0) {
                        aux++;
                    }
                }
                if (aux == 0) {
                    numeros[i] = j;
                    break;
                }
            }
        }
        System.out.print("Los 10 números primos entre 100 y 300 son: ");
        for (int i = 0; i < 10; i++) {
            System.out.print("" + numeros[i] + "\n");
        }

    }

    public int determinarNumMayorRepetido() {
        int pos = 0;
        int suma = 0;
        int mayor = numeros[0];
        for (int i = 1; i < numeros.length; i++) {

            if (numeros[i] > mayor) {
                pos = i;
                mayor = numeros[i];
            }

        }

        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == mayor) {
                suma = suma + 1;
            }
        }
        return suma;
    }

    public static int obtenerSumaDigitos(int numero) {
        if (numero < 0) {
            numero = numero * (-1);
        }
        int suma = 0;
        while (numero > 9) {
            suma = suma + (numero % 10);
            numero = numero / 10;
        }
        suma = suma + numero;
        return suma;
    }

    public int[] obtenerSumaDigitosPorPosicion() {

        int[] sumaDigitos = new int[numeros.length];
        for (int i = 0; i < numeros.length; i++) {
            sumaDigitos[i] = Utilidad.obtenerSumaDigitos(numeros[i]);
        }
        return sumaDigitos;
    }

    public long sumarDatos() {
        long acumulador = 0;

        for (int i = 0; i < numeros.length; i++) {
            acumulador += numeros[i];
        }

        return acumulador;
    }

    public double promediarDatos() {
        return this.sumarDatos() / (double) numeros.length;
    }

    public String determinarPromedioEnArreglo() {

        String si = "";

        for (int i = 0; i < numeros.length; i++) {

            if (promediarDatos() == numeros[i]) {
                si = "El promedio si está en el arreglo";
            }
        }
        return si;
    }

    public String mostrarDatosFactorial() {

        String texto = "";

        for (int i = 0; i < factorial.length; i++) {

            texto = texto + "\nNumero[" + i + "]: " + factorial[i];
        }
        return texto;
    }

    public void determinarFactorial() {

        for (int i = 1; i < factorial.length; i++) {
            long cont = 1;

            for (int j = 1; j <= factorial[i]; j++) {

                cont = cont * j;
            }

            factorial[i] = cont;

            System.out.println(factorial[i]);
        }

    }

    public void mostrarDatosPuntoNueve() {

        for (int i = 0; i < 10; i++) {
            System.out.print("\n·Los enteros comprendidos entre 1 y " + numeros[i] + " son: ");
            for (int j = 2; j < numeros[i]; j++) {
                System.out.print("" + j + ", ");
            }
        }
    }

}
